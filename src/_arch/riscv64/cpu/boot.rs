extern crate riscv_rt;

use riscv::asm::wfi;
use riscv::register::mhartid;

use riscv_rt::entry;

#[export_name = "_mp_hook"]
#[rustfmt::skip]
pub extern "Rust" fn user_mp_hook() -> bool {
    let hartid = mhartid::read();
    if hartid == 0 {
        true
    } else {
        false
    }
}

#[entry]
fn arch_init() -> ! {
    let hartid = mhartid::read();

    if hartid == 0 {
        unsafe { crate::kernel_init() }
    }

    loop {
        unsafe {
            wfi();
        }
    }
}
