extern crate riscv;
extern crate riscv_rt;

// start of RISC-V CPU support
use riscv::asm;

//--------------------------------------------------------------------------------------------------
// Public Code
//--------------------------------------------------------------------------------------------------

pub use asm::nop;

/// Spin for `n` cycles.
#[inline(always)]
#[allow(unused)]
pub fn spin_for_cycles(n: usize) {
    for _ in 0..n {
        unsafe {
            asm::nop();
        }
    }
}

/// Pause execution on the core.
#[inline(always)]
#[allow(unused)]
pub fn wait_forever() -> ! {
    loop {
        unsafe { asm::wfi() }
    }
}
