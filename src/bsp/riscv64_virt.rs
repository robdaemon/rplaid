pub mod console;
pub mod driver;
pub mod memory;

/// Board identification.
pub fn board_name() -> &'static str {
    #[cfg(feature = "bsp_riscv64_virt")]
    {
        "QEMU Virt"
    }
}
