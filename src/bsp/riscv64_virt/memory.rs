#[rustfmt::skip]
pub(super) mod map {
    pub const UART_OFFSET:         usize = 0x0;

    /// Physical devices.
    #[cfg(feature = "bsp_riscv64_virt")]
    pub mod mmio {
        use super::*;

        pub const START:           usize =         0x1000_0000;
        pub const VIRT_UART_START: usize = START + UART_OFFSET;
    }
}
