// SPDX-License-Identifier: MIT OR Apache-2.0
//
// Copyright (c) 2018-2022 Andre Richter <andre.o.richter@gmail.com>

//! Device driver.

mod common;

#[cfg(any(feature = "bsp_rpi3", feature = "bsp_rpi4"))]
mod bcm;

#[cfg(any(feature = "bsp_rpi3", feature = "bsp_rpi4"))]
pub use bcm::*;

#[cfg(any(feature = "bsp_riscv64_virt"))]
mod virt;

#[cfg(any(feature = "bsp_riscv64_virt"))]
pub use virt::*;
