// SPDX-License-Identifier: MIT OR Apache-2.0
//
// Copyright (c) 2022 Robert Roland <robert@robertroland.org>

//! NS16550A UART driver
//!
//! # Resources
//!
//! - <http://bitsavers.trailing-edge.com/components/national/_appNotes/AN-0491.pdf>

use crate::{
    bsp::device_driver::common::MMIODerefWrapper, console, cpu, driver, synchronization,
    synchronization::NullLock,
};
use core::fmt;
use tock_registers::{
    interfaces::{Readable, Writeable},
    register_bitfields, register_structs,
    registers::{Aliased, ReadOnly, ReadWrite},
};

// Register definitions
register_bitfields! {
    u8,

    // Interrupt Identification Register
    IER [
        // Modem status interrupt enable
        MS_INT_EN OFFSET(3) NUMBITS(1) [
            DISABLED = 0,
            ENABLED = 1,
        ],
        // Receiver line status interrupt enable
        RLS_INT_EN OFFSET(2) NUMBITS(1) [
            DISABLED = 0,
            ENABLED = 1,
        ],
        // Transmit holding register interrupt enable
        THRE_INT_EN OFFSET(1) NUMBITS(1) [
            DISABLED = 0,
            ENABLED = 1,
        ],
        // Receive data available interrupt enable
        RDA_INT_EN OFFSET(0) NUMBITS(1) [
            DISABLED = 0,
            ENABLED = 1,
        ],
    ],

    // Interrupt Identification Register
    IIR [
        // Timeout interrupt
        TIMEOUT_INT OFFSET(4) NUMBITS(1) [],
        // Interrupt priority
        INT_PRIO OFFSET(2) NUMBITS(2) [],
        // Interrupt pending
        INT_PENDING OFFSET(0) NUMBITS(1) [],
    ],

    // FIFO Control Register
    FCR [
        // Trigger level of Received Data Available interrupt
        TRIG_LVL OFFSET(7) NUMBITS(2) [
            ONE_BYTES = 0x00,
            FOUR_BYTES = 0x40,
            EIGHT_BYTES = 0x80,
            FOURTEEN_BYTES = 0xC0,
        ],
        // Resets the XMIT FIFO
        XMIT_FIFO_RST OFFSET(2) NUMBITS(1) [],
        // Resets the RCVR FIFO
        RCVR_FIFO_RST OFFSET(1) NUMBITS(1) [],
        FIFO_EN OFFSET(0) NUMBITS(1) [
            DISABLED = 0,
            ENABLED = 1,
        ],
    ],

    // Line Control Register
    LCR [
        // Divisor Latch Access bit
        DLAB OFFSET(7) NUMBITS(1) [],
        // Break Control Enable
        BREAK_CTRL_EN OFFSET(6) NUMBITS(1) [
            DISABLED = 0,
            ENABLED = 1,
        ],
        // Stick Parity bit
        STICK_PARITY_EN OFFSET(5) NUMBITS(1) [
            DISABLED = 0,
            ENABLED = 1,
        ],
        // Even Parity Select bit
        EVEN_PARITY_EN OFFSET(4) NUMBITS(1) [
            DISABLED = 0,
            ENABLED = 1,
        ],
        // Parity Enable bit
        PARITY_EN OFFSET(3) NUMBITS(1) [
            DISABLED = 0,
            ENABLED = 1,
        ],
        // Stop Bit Control
        STOP_BIT_CTRL OFFSET(2) NUMBITS(1) [
            ONE = 0,
            TWO = 1,
        ],
        // Character length select
        CHAR_LEN_SEL OFFSET(1) NUMBITS(2) [
            FIVE_BITS = 0b00,
            SIX_BITS = 0b01,
            SEVEN_BITS = 0b10,
            EIGHT_BITS = 0b11,
        ],
    ],

    // Modem Control Register
    MCR [
        // Provides a local loopback feature for diagnostic testing of the UART
        LOOPBACK_EN OFFSET(4) NUMBITS(1) [
            DISABLED = 0,
            ENABLED = 1,
        ],
        // Auxiliary output 2 control
        OUT2_CTRL OFFSET(3) NUMBITS(1) [],
        // Auxiliary output 1 control.
        OUT1_CTRL OFFSET(2) NUMBITS(1) [],
        // Request to Send Output Control
        RTS_CTRL OFFSET(1) NUMBITS(1) [
            DISABLED = 0,
            ENABLED = 1,
        ],
        // Data Terminal Ready Output Control
        DTR_CTRL OFFSET(0) NUMBITS(1) [
            DISABLED = 0,
            ENABLED = 1,
        ],
    ],

    // Line Status Register
    LSR [
        FIFO_ERR OFFSET(7) NUMBITS(1) [
            NO = 0,
            YES = 1,
        ],
        XMITR_EMPTY OFFSET(6) NUMBITS(1) [
            NO = 0,
            YES = 1,
        ],
        THR_EMPTY OFFSET(5) NUMBITS(1) [
            NO = 0,
            YES = 1,
        ],
        BREAK_COND OFFSET(4) NUMBITS(1) [
            NO = 0,
            YES = 1,
        ],
        FRAMING_ERR OFFSET(3) NUMBITS(1) [
            NO = 0,
            YES = 1,
        ],
        PARITY_ERR OFFSET(2) NUMBITS(1) [
            NO = 0,
            YES = 1,
        ],
        OVERRUN_ERR OFFSET(1) NUMBITS(1) [
            NO = 0,
            YES = 1,
        ],
        DATA_RDY OFFSET(0) NUMBITS(1) [
            NO = 0,
            YES = 1,
        ],
    ],

    // Modem Status Register
    MSR [
        CDCDI OFFSET(7) NUMBITS(1) [],
        CRI OFFSET(6) NUMBITS(1) [],
        CDSR OFFSET(5) NUMBITS(1) [],
        CCTS OFFSET(4) NUMBITS(1) [],
        DDCDI OFFSET(3) NUMBITS(1) [],
        TERI OFFSET(2) NUMBITS(1) [],
        DDSR OFFSET(1) NUMBITS(1) [],
    ],
}

register_structs! {
    #[allow(non_snake_case)]
    pub RegisterBlock {
        (0x00 => HR: ReadWrite<u8>),
        (0x01 => IER: ReadWrite<u8, IER::Register>),
        (0x02 => IIR_FCR: Aliased<u8, IIR::Register, FCR::Register>),
        (0x03 => LCR: ReadWrite<u8, LCR::Register>),
        (0x04 => MCR: ReadWrite<u8, MCR::Register>),
        (0x05 => LSR: ReadOnly<u8, LSR::Register>),
        (0x06 => MSR: ReadOnly<u8, MSR::Register>),
        (0x07 => SCR: ReadWrite<u8>),
        (0x08 => @END),
    }
}

/// Abstraction for the associated MMIO registers.
type Registers = MMIODerefWrapper<RegisterBlock>;

#[derive(PartialEq)]
enum BlockingMode {
    Blocking,
    NonBlocking,
}

struct VirtUartInner {
    registers: Registers,
    chars_written: usize,
    chars_read: usize,
}

pub struct VirtUart {
    inner: NullLock<VirtUartInner>,
}

impl VirtUartInner {
    pub const unsafe fn new(mmio_start_addr: usize) -> Self {
        Self {
            registers: Registers::new(mmio_start_addr),
            chars_written: 0,
            chars_read: 0,
        }
    }

    pub fn init(&mut self) {
        // set the word length to 8 bits
        self.registers.LCR.write(LCR::CHAR_LEN_SEL::EIGHT_BITS);

        // setting up polling / enable FIFO
        self.registers.IIR_FCR.write(FCR::FIFO_EN::SET);

        self.registers.IER.write(IER::MS_INT_EN::DISABLED);
        self.registers.IER.write(IER::RLS_INT_EN::DISABLED);
        self.registers.IER.write(IER::THRE_INT_EN::DISABLED);
        self.registers.IER.write(IER::RDA_INT_EN::DISABLED);
    }

    fn write_char(&mut self, c: char) {
        while self.registers.LSR.matches_all(LSR::THR_EMPTY::NO) {
            unsafe {
                cpu::nop();
            }
        }

        self.registers.HR.set(c as u8);

        self.chars_written += 1;
    }

    fn flush(&self) {
        while self.registers.LSR.matches_all(LSR::XMITR_EMPTY::NO) {
            unsafe {
                cpu::nop();
            }
        }
    }

    fn read_char_converting(&mut self, _blocking_mode: BlockingMode) -> Option<char> {
        while self.registers.LSR.matches_all(LSR::DATA_RDY::NO) {
            if _blocking_mode == BlockingMode::NonBlocking {
                return None;
            }

            unsafe {
                cpu::nop();
            }
        }

        let mut ret = self.registers.HR.get() as char;

        if ret == '\r' {
            ret = '\n';
        }

        self.chars_read += 1;

        Some(ret)
    }
}

impl fmt::Write for VirtUartInner {
    fn write_str(&mut self, s: &str) -> fmt::Result {
        for c in s.chars() {
            self.write_char(c);
        }

        Ok(())
    }
}

impl VirtUart {
    pub const COMPATIBLE: &'static str = "QEMU VirtIO UART";

    pub const unsafe fn new(mmio_start_addr: usize) -> Self {
        Self {
            inner: NullLock::new(VirtUartInner::new(mmio_start_addr)),
        }
    }
}

use synchronization::interface::Mutex;

impl driver::interface::DeviceDriver for VirtUart {
    fn compatible(&self) -> &'static str {
        Self::COMPATIBLE
    }

    unsafe fn init(&self) -> Result<(), &'static str> {
        self.inner.lock(|inner| inner.init());

        Ok(())
    }
}

impl console::interface::Write for VirtUart {
    fn write_char(&self, c: char) {
        self.inner.lock(|inner| inner.write_char(c));
    }

    fn write_fmt(&self, args: core::fmt::Arguments) -> fmt::Result {
        self.inner.lock(|inner| fmt::Write::write_fmt(inner, args))
    }

    fn flush(&self) {
        // Spin until TX FIFO is empty
        self.inner.lock(|inner| inner.flush());
    }
}

impl console::interface::Read for VirtUart {
    fn read_char(&self) -> char {
        self.inner
            .lock(|inner| inner.read_char_converting(BlockingMode::Blocking).unwrap())
    }

    fn clear_rx(&self) {
        // read from RX FIFO until empty
        while self
            .inner
            .lock(|inner| inner.read_char_converting(BlockingMode::NonBlocking))
            .is_some()
        {}
    }
}

impl console::interface::Statistics for VirtUart {
    fn chars_written(&self) -> usize {
        self.inner.lock(|inner| inner.chars_written)
    }

    fn chars_read(&self) -> usize {
        self.inner.lock(|inner| inner.chars_read)
    }
}

impl console::interface::All for VirtUart {}
